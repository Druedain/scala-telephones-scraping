package wicu.telephones

import java.text.SimpleDateFormat
import java.util.Date

/**
 * @author wicu
 */
object Files {

  /**
   * ".dat" file extension.
   */
  val DatExt = ".dat"

  val TodaysFileName = {
    val dateFormat = new SimpleDateFormat("yyMMdd")
    val todaysFileName = dateFormat.format(new Date())

    todaysFileName
  }
}
