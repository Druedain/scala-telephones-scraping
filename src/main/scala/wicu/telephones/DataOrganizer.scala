package wicu.telephones

import java.io.File
import java.io.PrintWriter
import scala.BigDecimal
import scala.collection.parallel.ParIterable
import scala.collection.parallel.ParMap
import scala.collection.parallel.ParSeq
import scala.language.postfixOps
import scala.util.matching.Regex
import Types._
import org.jsoup.Jsoup
import scala.collection.JavaConversions.asScalaBuffer

/**
 * @author wicu
 */
object DataOrganizer {

  def getData() {
    val specsByFeaturesByModelsUrlsByBrandsUrls: SpecsByFeaturesByModelUrlsByBrandUrls = DataAdapter.getData
    val modelInfos: ParSeq[ModelInfo] = getModelInfosFrom(specsByFeaturesByModelsUrlsByBrandsUrls) toSeq

    val rows: ParSeq[String] = modelInfos map (_.asEntry)
    val allBrandsAllModelsInfosCSV: String = rows reduce (joinAtNewLine)

    val writer = new PrintWriter(new File("telephones.csv"))
    writer write ModelInfo.columns
    writer write allBrandsAllModelsInfosCSV
  }

  def getModelInfosFrom(specsByFeaturesByModelUrlsByBrandUrls: SpecsByFeaturesByModelUrlsByBrandUrls): ParIterable[ModelInfo] = {
    val modelInfos: ParIterable[ModelInfo] = specsByFeaturesByModelUrlsByBrandUrls map { case (brandUrl, value) => asModelInfos(brandUrl, value) } flatten

    return modelInfos
  }

  def asModelInfos(brandUrl: String, specsByFeaturesByModelsUrls: ParMap[String, ParMap[String, String]]): ParSeq[ModelInfo] = {
    val modelInfos: ParSeq[ModelInfo] = specsByFeaturesByModelsUrls map { case (modelUrl, specsByFeatures) => extractModelInfoFrom(brandUrl, modelUrl, specsByFeatures) } toSeq

    return modelInfos
  }

  def extractModelInfoFrom(brandUrl: String, modelUrl: String, specsByFeatures: ParMap[String, String]): ModelInfo = {
    val brand = getBrandFrom(brandUrl)
    val model = getModelFrom(modelUrl, brand)

    val maybeResolution = specsByFeatures get ("Resolution")
    if (maybeResolution isEmpty) {
      sys.error(s"Nie ma rozdzielczości w $modelUrl")
    }
    val (x, y) = extractResolutionFrom(maybeResolution get, modelUrl)

    val maybeScr = specsByFeatures get ("Size")
    if (maybeScr isEmpty) {
      sys.error(s"Nie ma przekątnej w $modelUrl")
    }
    val scr = extractScreenFrom(maybeScr get, modelUrl)

    val maybeDate = specsByFeatures get ("Announced")
    if (maybeDate isEmpty) {
      sys.error(s"Nie ma daty w $modelUrl")
    }
    val (year, month) = extractDateFrom(maybeDate get, modelUrl)

    val modelInfo = new ModelInfo(brand, model, x, y, scr, year, month, modelUrl)

    return modelInfo
  }

  def extractResolutionFrom(resolution: String, modelUrl: String): (Int, Int) = {
    val parts = (new Regex("[0-9]+") findAllIn (resolution) toSeq)
    try {
      val (x, y): (Int, Int) = (parts(0) toInt, parts(1) toInt)

      return (x, y)
    } catch {
      case e: NumberFormatException => {
        throw new RuntimeException(s"Błąd rozdzielczości w $modelUrl", e)
      }
      case aioobe @ (_: ArrayIndexOutOfBoundsException | _: IndexOutOfBoundsException) => {
//        println(s"""Ogarniam błąd rozdzielczości kiedy nie ma z czego jej poskładać "$resolution" -> $modelUrl""")
      }
    }

    return (0, 0)
  }

  def extractScreenFrom(scr: String, modelUrl: String): BigDecimal = {
    if (scr isEmpty ()) {
      return BigDecimal(0)
    }
    val onlyDigits = scr filter { _.isDigit }
    if (onlyDigits isEmpty) {
      return BigDecimal(0)
    }
    val real = (new Regex("[0-9]+.[0-9]+") findAllIn (scr) toSeq)
    if (real isEmpty) {
      return BigDecimal(0)
    }

    val size = real(0)

    return BigDecimal(size)
  }

  def joinAtNewLine(res: String, row: String): String = {
    return s"$res\n$row"
  }

  def extractDateFrom(date: String, modelUrl: String): (Int, String) = {
    if (date isEmpty ()) {
      return (0, "")
    }

    val parts = date split (",")
    val year = try {
      parts(0) toInt
    } catch {
      case npe: NumberFormatException =>
        {
//          println(s"""Data jest z dupy, nie zrobię roku z "$date" -> $modelUrl """)
        }

        0
    }

    if ((parts size) < 2) {
//      println(s"""Ogarniam błąd daty "$date" -> $modelUrl""")

      return (year, "")
    }

    val month = parts(1) replaceAll (" ", "")

    return (year, month)
  }

  def getBrandFrom(brandUrl: String): String = {
    val document = Jsoup connect (brandUrl) get
    val h1s = document getElementsByTag ("h2")
    val header = h1s iterator () next () text ()
    val fixed = header replaceAll ("phones", "") trim ()

    return fixed
  }

  def getModelFrom(modelUrl: String, brand: String): String = {
    val document = Jsoup connect (modelUrl) get
    val h1s = document getElementsByTag ("h1")
    val header = h1s iterator () next () text ()
    val fixed = header replaceAll (brand, "") trim ()

    return fixed
  }
}
