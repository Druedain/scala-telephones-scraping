package wicu.telephones

/**
 * @author wicu
 */

object ModelInfo {

  val columns = "marka;model;rozdzielczość X;rozdzielczość Y;przekątna ekranu;rok;miesiąc;odnośnik"
}

class ModelInfo(brand: String, model: String, resX: Int, resY: Int, scrSize: BigDecimal, year: Int, month: String, modelUrl: String) {
  def asEntry: String = {
    return s"$brand;$model;$resX;$resY;$scrSize;$year;$month;$modelUrl"
  }
}
