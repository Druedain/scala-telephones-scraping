package wicu.telephones

import java.io.File
import java.io.FileInputStream
import java.io.ObjectInputStream
import scala.language.postfixOps
import scala.collection.parallel.ParMap

import Types._

/**
 * If there is fresh data, this object provides it.
 *
 * @author wicu
 */
object DataFileExtractor {

  /**
   * Main method.
   */
  def getData: Option[SpecsByFeaturesByModelUrlsByBrandUrls] = {
    val dir = new File(".")
    val normalFiles: Seq[File] = (dir.listFiles() toSeq) filter (_.isFile)

    val suffixedDatFilesNames = normalFiles map (_.getName) filter (_.endsWith(Files.DatExt))
    if (suffixedDatFilesNames isEmpty) {
      return None
    }

    val newestFileName: String = suffixedDatFilesNames reduce (higherString) replaceAll (Files.DatExt, "")

    if (Files.TodaysFileName != newestFileName) {
      return None
    }

    return extractFrom(s"${Files.TodaysFileName}${Files.DatExt}")
  }

  /**
   * Gives lexicographically higher string. Useful in reduce when finding "highest" string.
   */
  def higherString(s1: String, s2: String): String = if (s1 > s2) s1 else s2

  /**
   * Extracts data from file of given name.
   */
  def extractFrom(fileName: String): Option[SpecsByFeaturesByModelUrlsByBrandUrls] = {
    val inputStream = new ObjectInputStream(new FileInputStream(fileName))
    val rawData = inputStream.readObject()
    rawData match {
      case x: SpecsByFeaturesByModelUrlsByBrandUrls => return Some(x)
      case _ => {
        println("Nie udało się odczytać danych z pliku :(")

        None
      }
    }
  }
}
